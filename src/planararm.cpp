#include "planararm.h"

PlanarArm::PlanarArm(Eigen::MatrixXd len, Eigen::MatrixXd maxL, Eigen::MatrixXd minL){
	lengthSeg = len;
	maxLimits = maxL;
	minLimits = minL;
}

Eigen::MatrixXd PlanarArm::toWorkSpace(Eigen::MatrixXd configSpace){
	Eigen::MatrixXd temp(1,2);
	temp<< 0, 0;
	double angle = 0;
	for(int i = 0; i < lengthSeg.cols(); i++){
		angle = angle + configSpace(0, i);
		temp(0, 0) = temp(0, 0) + lengthSeg(0, i) * cos(angle);
		temp(0, 1) = temp(0, 1) + lengthSeg(0, i) * sin(angle);
	}
	return temp;
}
Eigen::MatrixXd PlanarArm::toConfigSpace(Eigen::MatrixXd workSpace){
}

std::vector<fcl::CollisionGeometry*> PlanarArm::getModel(){
	fcl::CollisionGeometry* box;
	std::vector<fcl::CollisionGeometry*> temp;
	for(int i = 0; i < lengthSeg.cols(); i++){
		box = new fcl::Box(lengthSeg(0, i), width, 0);
		temp.push_back(box);
	}
	return temp;
}

Eigen::MatrixXd PlanarArm::getRandomConfig(){
	Eigen::MatrixXd temp(1, lengthSeg.cols());
	for(int i = 0; i < lengthSeg.cols(); i++)
		temp(0, i) = rand() / (float)RAND_MAX * (maxLimits(0, i) - minLimits(0, i)) + minLimits(0, i);
	return temp; 
}

Eigen::MatrixXd PlanarArm::getRandomConfig(double r){
	Eigen::MatrixXd temp(1, lengthSeg.cols());
	for(int i = 0; i < lengthSeg.cols(); i++)
		temp(0, i) = rand() / (float)RAND_MAX * (maxLimits(0, i) - minLimits(0, i)) + minLimits(0, i);
	return temp; 
}

std::vector<fcl::Transform3f> PlanarArm::getTransform(Eigen::MatrixXd state){
	fcl::Matrix3f R;
	fcl::Vec3f T;
	double angle = 0;
	double angle1 = 0;
	std::vector<fcl::Transform3f> temp;
	for(int i = 0; i < lengthSeg.cols(); i++){
		angle1 = angle1 + state(0, i);
		R = getRotationMatrix(angle1);
		fcl::Quaternion3f qa;
		qa.fromRotation(R);
		T[0] = 0;
		T[1] = 0;
		angle = 0;
		for(int j = 0; j < i; j++){
			angle = angle + state(0, j);
			T[0] = T[0] + lengthSeg(0, j) * cos(angle);
			T[1] = T[1] + lengthSeg(0, j) * sin(angle);
		}
		angle = angle + state(0, i);
		T[0] = T[0] + lengthSeg(0, i) / 2.0 * cos(angle);
		T[1] = T[1] + lengthSeg(0, i) / 2.0 * sin(angle);
		fcl::Transform3f tran(R, T);
		tran.setQuatRotation(qa);
		temp.push_back(tran);
	}
	return temp;
}

fcl::Matrix3f PlanarArm::getRotationMatrix(double angle){
	fcl::Matrix3f R;
	R(0, 0) = cos(angle);
	R(0, 1) = -sin(angle);
	R(1, 0) = sin(angle);
	R(1, 1) = cos(angle);
	return R;
}
Eigen::MatrixXd  PlanarArm::getNRandomConfig(int N, double dist){
	Eigen::MatrixXd t(N,2);
	return t;
}

int PlanarArm::getDimension(){
	return lengthSeg.cols();
}

#include "rrtsimple.h"

RRTsimple::RRTsimple(double step){
	stepSize = step;
}

std::vector<Node*> RRTsimple::extend(Eigen::MatrixXd target, Node* source) {
	std::vector<Node*> nodes;
	if (!source) {
        source = findNearest(target);
        if (!source) {
            return nodes;
        }
    }
    //calculating intermediate state
	Eigen::MatrixXd intermediateState(1, target.cols());
	Eigen::MatrixXd delta = target - source->getConfigState();
	delta = delta / delta.norm();   //  unit vector
	intermediateState = source->getConfigState() + delta * stepSize;
	//std::cout<<source->getConfigState()<<std::endl;
	//std::cout<<intermediateState<<std::endl;
	//std::cout<<target<<std::endl;
	Robot* robot = getUniverse()->getRobot();
	//std::cout<<"check"<<universe->checkMotionValidity(source->getConfigState(), intermediateState)<<std::endl;
	// check motion validity
	if (!universe->checkMotionValidity(source->getConfigState(), intermediateState) || !universe->getEnvironment()->checkEnvironmentFrame(robot->toWorkSpace(intermediateState)))
		return nodes;
	Node* n = new Node(intermediateState, source);
	tree->addNewNode(n);
	nodes.push_back(n);
	index->addPoints(flann::Matrix<double>(&((tree->getAllPoints())->back())(0,0), 1, target.cols()));
	return nodes;
}
std::vector<Node*> RRTsimple::extend_np(Node* nearestNode){
	std::vector<Node*> nodes;
	return nodes;
}

std::vector<Node*> RRTsimple::extend1(Node* nearestNode){
	std::vector<Node*> nodes;
	return nodes;
}

std::vector<Node*> RRTsimple::dod_tree(Node* nearestNode){
	std::vector<Node*> nodes;
	return nodes;
}

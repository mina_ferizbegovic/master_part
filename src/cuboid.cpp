#include "cuboid.h"

Cuboid::Cuboid(double x1, double y1, double z1, double a, double b, double c){
	this->a = a;
	this->b = b;
	this->c = c;
	this->x1 = x1;
	this->y1 = y1;
	this->z1 = z1;
	box =  new fcl::Box(a, b, c);
	fcl::Matrix3f R;
	R.setIdentity();
	fcl::Vec3f T(x1 + a / 2.0, y1 + b / 2.0, z1 + c / 2.0);
	transform = fcl::Transform3f(R, T);
}

fcl::CollisionGeometry* Cuboid::getModel(){
	return box;
}

fcl::Transform3f Cuboid::getTransform(){
	return transform;
}

void Cuboid::plotObstacle(){
}

std::vector<std::vector<double> > Cuboid::getPlotData(){
	std::vector<double> x(5), y(5);
	x.at(0) = x1;
	x.at(1) = x1;
	y.at(0) = y1;
	y.at(1) = y1 + b;
	x.at(2) = x1 + a;
	x.at(3) = x1 + a;
	y.at(2) = y1 + b;
	y.at(3) = y1;
	x.at(4) = x1;
	y.at(4) = y1;
	std::vector<std::vector<double> >temp;
	temp.push_back(x);
	temp.push_back(y);
	return temp;
}

fcl::CollisionObject* Cuboid::getObject(){
	return new fcl::CollisionObject(boost::shared_ptr<fcl::CollisionGeometry>(box), transform);
}

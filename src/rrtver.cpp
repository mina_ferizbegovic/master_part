#include "rrtver.h"
#include "matplotlibcpp.h"
namespace plt = matplotlibcpp;

void RRTver::setStartState(Eigen::MatrixXd start){
	startState = start;
	tree->setStartPosition(start);
	help_node->setParent(nullptr);
	int n = 1;
	index = new Index<L2<double> >(flann::Matrix<double>(&(tree->getAllPoints())->back()(0,0), 1, start.cols()), flann::KDTreeIndexParams(1));
	index->buildIndex();
	indices = new flann::Matrix<int> (new int[n], 1, n);
	dists = new flann::Matrix<double> (new double[n], 1, n);
}

int RRTver::getStopIter(){
	return iter;
}
void RRTver::setGoalState(Eigen::MatrixXd goal){
	goalState = goal;
}

void RRTver::setPoints(Eigen::MatrixXd B){
	A = B;
}
Eigen::MatrixXd RRTver::getRandomConfig(){
	return universe->getValidRandomConfig();
}

std::vector<Node*> RRTver::grow(){
	iter++;
	//if(nap) extend(help_node->getConfigState(), help_node->getParent());
	if(ror) return extend(universe->getValidRandomConfig(), help_node);
	if (iter % 50 == 0)
        return extend(goalState, nullptr);
	else
		return extend(getRandomConfig(), nullptr);
}

void RRTver::setName(std::string a){
	name = a;
}

int RRTver::getNodeNum(){
	return tree->getAllNodes().size();
}

int RRTver::getRejected(){
	return 1;
}
int RRTver::getCollisionChecking(){
	return universe->getCollisionCheck();
}

bool RRTver::run() {
    //  grow the tree until we find the goal or run out of iterations
    for (int i = 0; i < maxIterations; i++) {
        std::vector<Node*> nodes = grow();
				//std::cout<<"ror"<<ror<<std::endl;
        Eigen::MatrixXd zt(1,2);
        zt<<3.3, 4.9;
				//std::cout << "nap"<<nap <<std::endl;
			//	if(nap) return false;
			//std::cout<<i<<std::endl;
			//if(nap) return false;
        for(int j = 0; j < nodes.size(); j++){
			if((nodes.at(j)->getConfigState() - zt).norm() < 0.15 && npIter == -1) npIter = i;
			if (nodes.at(j) && (nodes.at(j)->getConfigState() - goalState).norm() < goalThresholdDistance){
				tree->setLastNode(nodes.at(j));
				 return true;
			 }
		}
	}
    return false;
}

Node* RRTver::findNearest(Eigen::MatrixXd randNode){
	index->knnSearch(flann::Matrix<double>(&randNode(0, 0), 1, randNode.cols()), *indices, *dists, 1, flann::SearchParams(128));
	double* uu = index->getPoint(*indices[0][0]);

	Eigen::MatrixXd temp(1, randNode.cols());
	for (int i = 0; i < randNode.cols(); i++){
		temp(0, i) = *uu;
		uu++;
	}
	Node* s = tree->getNode(temp);
	return s;
}

std::vector<Eigen::MatrixXd> RRTver::getConfigSpacePath(){
	return tree->getPath();
}

std::vector<Eigen::MatrixXd> RRTver::getWorkSpacePath(){
	std::vector<Eigen::MatrixXd> temp = tree->getPath();
	std::vector<Eigen::MatrixXd> v;
	for(int i = 0; i < temp.size(); i++)
		v.push_back(getUniverse()->getWorkSpace(temp[i]));
	return v;
}

void RRTver::plotRRTPath(){
	Eigen::MatrixXd temp = universe->getRobot()->toWorkSpace(goalState);
	if(temp.cols() < 3) plotRRTPath2D();
	else plotRRTPath3D();
}

void RRTver::plotRRTPath2D(){
	std::ofstream myfile1;
	myfile1.open("example1.txt");
	double r = goalThresholdDistance;
	double n = 100;
	Eigen::MatrixXd goal = universe->getRobot()->toWorkSpace(goalState);
	double xc = goal(0, 0);
	double yc = goal(0, 1);
	std::vector<double> x(n + 1), y(n + 1);
	for(int i = 0; i <= n; i++) {
		x.at(i) = r * cos(2 * M_PI * i / n) + xc;
		y.at(i) = r * sin(2 * M_PI * i / n) + yc;
	}

	plt::plot(x, y, "r");
	Eigen::MatrixXd start = universe->getRobot()->toWorkSpace(startState);
	xc = start(0, 0);
	yc = start(0, 1);
	for(int i=0; i <= n; ++i) {
		x.at(i) = r * cos(2 * M_PI * i / 100) + xc;
		y.at(i) = r * sin(2 * M_PI * i / 100) + yc;
	}
	plt::plot(x, y, "r");

	std::vector<double> x1(2), y1(2);
	//std::cout<<"mikica"<<std::endl;
	std::cout<<tree->getAllNodes().size()<<std::endl;

	for(int i = 1; i < tree->getAllNodes().size(); i++){
		Node* t = tree->getAllNodes().at(i);
		Eigen::MatrixXd prev, next;
		prev =  universe->getRobot()->toWorkSpace(t->getParent()->getConfigState());
		next = universe->getRobot()->toWorkSpace(t->getConfigState());
		//std::cout<<prev <<" "<<next<<std::endl;
		x1.at(0) =next(0, 0);
		y1.at(0) = next(0, 1);
		x1.at(1) = prev(0, 0);
		y1.at(1) = prev(0, 1);
		plt::plot(x1, y1, "b");

	}
	//std::cout<<"mikica"<<std::endl;
	std::vector<Eigen::MatrixXd> p = tree->getPath();
	Eigen::MatrixXd pp,pc;
	pp = universe->getRobot()->toWorkSpace(p.at(0));
	myfile1 << pp(0,0)<<" "<<pp(0,1)<<std::endl;
	for(int i = 1; i < p.size(); i++){
		pc = universe->getRobot()->toWorkSpace(p.at(i));
		x1.at(0) = pc(0, 0);
		y1.at(0) = pc(0, 1);
		x1.at(1) = pp(0, 0);
		y1.at(1) = pp(0, 1);
		plt::plot(x1, y1, "r");
		//myfile1 << pp(0,0)<<" "<<pp(0,1)<<std::endl;
		pp=pc;
	}

	std::vector<std::vector<std::vector<double> > > plotOb = universe->getEnvironment()->getPlotData();

	for(int i = 0; i < plotOb.size(); i++){
		plt::plot(plotOb.at(i).at(0), plotOb.at(i).at(1), "g");
	}

	std::vector<double> xe(n + 1), ye(n + 1), xm(n + 1), ym(n + 1);
	double ang = 0;
	for (auto& el: tree->getPCAhash()) {
		ang = el.second.getEigVectors()(1, 0) / el.second.getEigVectors()(0, 0);
		ang = atan(ang);
		for(int i = 0; i <=n; i++) {
			xe.at(i) = el.second.getEigValues()(0, 0) * cos(2 * M_PI * i / n)*cos(ang) -  el.second.getEigValues()(1, 0)  * sin(2 * M_PI * i / n) * sin(ang) + el.second.getAverage()(0,0);
			ye.at(i) = el.second.getEigValues()(0, 0) * cos(2 * M_PI * i / n)*sin(ang) +  el.second.getEigValues()(1, 0)  * sin(2 * M_PI * i / n) * cos(ang) + el.second.getAverage()(0,1);
		myfile1 << xe.at(i) <<" "<<ye.at(i)<<std::endl;
		}


	plt::plot(xe, ye, "g");
	}

	/*double amp = 2;
	double f = 0.25;
	double bias1 = 6;
	double bias2 = 5.0;


	for(int i = 0; i <= n; i++) {
			xm.at(i) = i / n * 10.0;
			ym.at(i) = amp * sin(2 * M_PI * f * i / n * 10.0) + bias1;
		}
	plt::plot(xm, ym, "b");

		for(int i = 0; i <= n; i++) {
			xm.at(i) = i / n * 10.0;
			ym.at(i) = amp * sin(2 * M_PI * f * i / n * 10.0) + bias2;
		}
	plt::plot(xm, ym, "b");*/

	plt::title("Work space data");
	// save figure
	plt::save(name);
	myfile1.close();

}

void RRTver::print(){
	std::ofstream myfile;
	myfile.open ("example.txt");
	//myfile1.open ("example1.txt");
	for(int i = 1; i < tree->getAllNodes().size(); i++){
		Node* t = tree->getAllNodes().at(i);
		Eigen::MatrixXd prev, next;
		prev =  universe->getRobot()->toWorkSpace(t->getParent()->getConfigState());
		next = universe->getRobot()->toWorkSpace(t->getConfigState());
		myfile << next <<" "<<prev<<std::endl;
	}
	int n = 100;
	std::vector<double> xe(n + 1), ye(n + 1), xm(n + 1), ym(n + 1);
		std::vector<double> x1(2), y1(2);
	double ang = 0;
	/*for (auto& el: tree->getPCAhash()) {
		ang = el.second.getEigVectors()(1, 0) / el.second.getEigVectors()(0, 0);
		ang = atan(ang);

		if(ang < 0) ang = M_PI + ang;
		for(int i = 0; i < n; i++) {
			xe.at(i) = el.second.getEigValues()(0, 0) * cos(2 * M_PI * i / n)*cos(ang) -  el.second.getEigValues()(1, 0)  * sin(2 * M_PI * i / n) * sin(ang) + el.second.getAverage()(0,0);
			ye.at(i) = el.second.getEigValues()(0, 0) * cos(2 * M_PI * i / n)*sin(ang) +  el.second.getEigValues()(1, 0)  * sin(2 * M_PI * i / n) * cos(ang) + el.second.getAverage()(0,1);
			myfile1 << xe.at(i) <<" "<<ye.at(i)<<std::endl;
		}
	}
	/*std::vector<Eigen::MatrixXd> p = tree->getPath();
	Eigen::MatrixXd pp,pc;
	std::cout<<p.at(0)<<std::endl;
	pp = universe->getRobot()->toWorkSpace(p.at(0));
		std::cout<<"sc"<<std::endl;
	//myfile1 << pp(0, 0) <<" "<<pp(0, 1)<<std::endl;
		std::cout<<"rc"<<std::endl;
	/*for(int i = 1; i < p.size(); i++){
		pc = universe->getRobot()->toWorkSpace(p.at(i));
		x1.at(0) = pc(0, 0);
		y1.at(0) = pc(0, 1);
		x1.at(1) = pp(0, 0);
		y1.at(1) = pp(0, 1);
		plt::plot(x1, y1, "r");
		pp=pc;
		std::cout<<"st"<<std::endl;

	}*/
	myfile.close();
	//myfile1.close();
	std::cout<<"r"<<std::endl;
}
void RRTver::plotRRTPath3D(){
	std::ofstream myfile, myfile1;
	myfile.open("example.txt");
	myfile1.open("example1.txt");
	for(int i = 1; i < tree->getAllNodes().size(); i++){
		Node* t = tree->getAllNodes().at(i);
		Eigen::MatrixXd prev, next;
		prev =  universe->getRobot()->toWorkSpace(t->getParent()->getConfigState());
		next = universe->getRobot()->toWorkSpace(t->getConfigState());
		myfile<< next <<" "<<prev<<std::endl;
	}
	std::vector<Eigen::MatrixXd> p = tree->getPath();
	Eigen::MatrixXd pp,pc;
	pp = universe->getRobot()->toWorkSpace(p.at(0));
	myfile1 << pp(0,0)<<" "<<pp(0,1)<<" "<<pp(0,2)<<std::endl;
		std::vector<double> x1(3), y1(3);
	for(int i = 1; i < p.size(); i++){
		pc = universe->getRobot()->toWorkSpace(p.at(i));
		myfile1 << pp(0,0)<<" "<<pp(0,1)<< " "<<pp(0,2)<<std::endl;
		pp=pc;
	}

	myfile1.close();
	myfile.close();

}

int RRTver::getNPenter(){
	return npIter;
}

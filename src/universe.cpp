#include "universe.h"

Universe::Universe(){
}

Universe::Universe(Environment* env, Robot* rob){
	setRobot(rob);
	setEnvironment(env);
	int N = 10000000;
	/*occupiedPoints.reserve(10000000);
	Eigen::MatrixXd randPoints(N, rob->getDimension());
	for(int j = 0; j < rob->getDimension(); j++){
		randPoints.col(j) = Eigen::MatrixXd::Random(N, 1) * 5 + Eigen::MatrixXd::Ones(N, 1)*5;
	}
	int n = 1;
	bool rt = 1;
	for(int i = 0; i < N; i++){
		if(!checkState(randPoints.row(i))){
			occupiedPoints.push_back(randPoints.row(i));
			if (rt){
				index_occ = new Index<L2<double> >(flann::Matrix<double>(&(occupiedPoints.back())(0,0), 1, rob->getDimension()), flann::KDTreeIndexParams(1));
				index_occ->buildIndex();
				rt = 0;
				indices_occ = new flann::Matrix<int> (new int[n], 1, n);
				dists_occ = new flann::Matrix<double> (new double[n], 1, n);
			}
			else
			{
				index_occ->addPoints(flann::Matrix<double>(&(occupiedPoints.back())(0,0), 1, rob->getDimension()));
			}
		}
	}*/
}

double Universe::findNearestObstacle(Eigen::MatrixXd randNode){

	// construct an randomized kd-tree index using 4 kd-trees
	// do a knn search, using 128 checks
	index_occ->knnSearch(flann::Matrix<double>(&randNode(0, 0), 1, randNode.cols()), *indices_occ, *dists_occ, 1, flann::SearchParams(128));
	double* uu = index_occ->getPoint(*indices_occ[0][0]);
	Eigen::MatrixXd temp(1, randNode.cols());
	for (int i = 0; i < randNode.cols(); i++){
		temp(0, i) = *uu;
		uu++;
	}
	std::cout<<randNode<<" "<<temp<<std::endl;
	return (randNode - temp).norm();
}

void Universe::setRobot(Robot* t){
	robot = t;
}

void Universe::setEnvironment(Environment* e){
	env = e;
}
void Universe::resetCollisionCheck(){
	colCheck = 0;
}

bool Universe::checkState(Eigen::MatrixXd state){
    if(state(0, 0) > 10 || state(0, 0) < 0 || state(0, 1) > 10 || state(0, 1) < 0) return false;
		double amp = 0.4;
		double f = 0.74;
		double bias1 = 5.0;
		double bias2 = 4.8;
		double y1 = amp * sin(2 * M_PI * f * state(0, 0)) + bias1;
		double y2 = amp * sin(2 * M_PI * f * state(0, 0)) + bias2;
		if(state(0, 0) >= 3.1 && state(0, 0) <= 8.5 && ((y1  < state(0, 1) && y1 + 1 > state(0, 1)) || (y2  > state(0, 1) && y2 - 1  < state(0, 1))))
			return false;
	//|| state(0, 2) > 10 || state(0, 2) < 0
	//return (state(0,0) - 5)*(state(0,0) - 5)/9 + (state(0,1) - 5)*(state(0,1) - 5)/1 > 1;
	std::vector<fcl::CollisionGeometry*> robCol = robot->getModel();
	std::vector<fcl::CollisionGeometry*> envCol = env->getModel();
	std::vector<fcl::Transform3f> robTr = robot->getTransform(state);
	std::vector<fcl::Transform3f> envTr = env->getTransform();
	fcl::CollisionRequest request;
	fcl::CollisionResult result;
	for(int i = 0; i < robCol.size(); i++){
		for(int j = 0; j < envCol.size(); j++){
			if(fcl::collide(robCol.at(i), robTr.at(i), envCol.at(j), envTr.at(j),request, result)) return false;
		}
	}
	return true;
}

/*bool Universe::checkState(Eigen::MatrixXd state){
	if(state(0, 0) > 10 || state(0, 0) < 0 || state(0, 1) > 10 || state(0, 1) < 0) return false;
	double amp = 2;
	double f = 0.25;
	double bias1 = 6;
	double bias2 = 5.0;
	if(amp * sin(2 * M_PI * f * state(0, 0)) + bias1 > state(0, 1) && amp * sin(2 * M_PI * f * state(0, 0)) + bias2 < state(0, 1))
		return true;
	return false;
}*/


bool Universe::checkMotionValidity(Eigen::MatrixXd currentState, Eigen::MatrixXd nextState){
	colCheck++;
	Eigen::MatrixXd state(1, currentState.cols());
	Eigen::MatrixXd delta = nextState - currentState;
	double stepSize = delta.norm();
	delta = delta / delta.norm();   //  unit vector
	state = currentState;
	int co = 100;
	for(int i = 1; i <= stepSize * co; i++){
		if(!checkState(state)) return false;
		state = state + delta / co;
	}
	return true;
}

/*bool Universe::checkMotionValidity(Eigen::MatrixXd currentState, Eigen::MatrixXd nextState){
	colCheck++;
	int iter = (nextState - currentState).norm()*100;
	fcl::ContinuousCollisionRequest requestc(iter);
    fcl::ContinuousCollisionResult resultc;
    fcl::Vec3f T(0,0,0);
	std::vector<fcl::CollisionGeometry*> robCol = robot->getModel();
	std::vector<fcl::CollisionGeometry*> envCol = env->getModel();
	std::vector<fcl::Transform3f> robTr = robot->getTransform(currentState);
	std::vector<fcl::Transform3f> robTrNext = robot->getTransform(nextState);
	std::vector<fcl::Transform3f> envTr = env->getTransform();
	boost::shared_ptr<fcl::CollisionGeometry>robCol1(robCol[0]);
	boost::shared_ptr<fcl::TranslationMotion> pom(new fcl::TranslationMotion(robTr[0], robTrNext[0]));
	std::vector<bool> insides = env->getIfInside();
	fcl::ContinuousCollisionObject* obj3 = new fcl::ContinuousCollisionObject(robCol1, pom);
	fcl::ContinuousCollisionObject* obj4;
	for(int i = 0; i < envCol.size(); i++){
		obj4 = new fcl::ContinuousCollisionObject(boost::shared_ptr<fcl::CollisionGeometry>(envCol[i]), boost::shared_ptr<fcl::TranslationMotion>(new fcl::TranslationMotion(envTr[i], envTr[i])));
		fcl::collide(obj3, obj4, requestc, resultc);
		if(resultc.is_collide) return false;
	}
	return true;
}
*/
Eigen::MatrixXd Universe::getValidRandomConfig(){
	Robot* s = getRobot();
	Eigen::MatrixXd temp = robot->getRandomConfig();
	/*while(!checkState(temp))
		temp = robot->getRandomConfig();*/
	return  temp;
}

Eigen::MatrixXd  Universe::getValidRandomConfig(double r){
	Robot* s = getRobot();
	Eigen::MatrixXd temp = robot->getRandomConfig(r);
	/*while(!checkState(temp))
		temp = robot->getRandomConfig();*/
	return  temp;
}

Eigen::MatrixXd Universe::getWorkSpace(Eigen::MatrixXd state){
	return robot->toWorkSpace(state);
}

Robot* Universe::getRobot(){
	return robot;
}

Environment* Universe::getEnvironment(){
	return env;
}
std::vector<Eigen::MatrixXd>* Universe:: getOccupiedPoints(){
	return &occupiedPoints;
}
/*Eigen::MatrixXd Universe::getState(Eigen::MatrixXd currentState, Eigen::MatrixXd nextState, double r, Eigen::MatrixXd goal){
	Eigen::MatrixXd state(1, currentState.cols());
	Eigen::MatrixXd state_prev(1, currentState.cols());
	Eigen::MatrixXd delta = nextState - currentState;
	double stepSize = delta.norm();
	delta = delta / delta.norm();   //  unit vector
	state = currentState;
	state_prev = currentState;
	double res = 0.1;
	if(!checkState(state + delta * res)) res = res / 4;
	while((currentState - state).norm()<=r){
		if(!checkState(state))
			return state_prev ;
		state_prev = state;
		state = state + delta * res;
		if((state - goal).norm() < 0.5) return state;
	}
	std::cout<<"sp"<<state_prev<<std::endl;
	return state_prev;
}*/
/*Eigen::MatrixXd Universe::getState(Eigen::MatrixXd currentState, Eigen::MatrixXd nextState, double r, Eigen::MatrixXd goal){
	Eigen::MatrixXd state(1, currentState.cols());
	Eigen::MatrixXd state_prev(1, currentState.cols());
	Eigen::MatrixXd delta = nextState - currentState;
	delta = delta / delta.norm();   //  unit vector
	state = currentState + r * delta;
	int iter = (state - currentState).norm()*100;
	fcl::ContinuousCollisionRequest requestc(iter);
    fcl::ContinuousCollisionResult resultc;
	std::vector<fcl::CollisionGeometry*> robCol = robot->getModel();
	std::vector<fcl::CollisionGeometry*> envCol = env->getModel();
	std::vector<fcl::Transform3f> robTr = robot->getTransform(currentState);
	std::vector<fcl::Transform3f> robTrNext = robot->getTransform(state);
	std::vector<fcl::Transform3f> envTr = env->getTransform();
	boost::shared_ptr<fcl::CollisionGeometry>robCol1(robCol[0]);
	boost::shared_ptr<fcl::TranslationMotion> pom(new fcl::TranslationMotion(robTr[0], robTrNext[0]));

	fcl::ContinuousCollisionObject* obj3 = new fcl::ContinuousCollisionObject(robCol1, pom);
	fcl::ContinuousCollisionObject* obj4;
	Eigen::MatrixXd ret = state;
	Eigen::MatrixXd retP = state;
	int j = 0;
	for(int i = 0; i < envCol.size(); i++){
		obj4 = new fcl::ContinuousCollisionObject(boost::shared_ptr<fcl::CollisionGeometry>(envCol[i]), boost::shared_ptr<fcl::TranslationMotion>(new fcl::TranslationMotion(envTr[i], envTr[i])));
		fcl::collide(obj3, obj4, requestc, resultc);
		if(resultc.is_collide){
			j ++;
			fcl::Vec3f pomT = resultc.contact_tf1.getTranslation();
			retP(0, 0) = pomT[0];
			retP(0, 1) = pomT[1];
			if((ret-state).norm() > (retP - state).norm() || j==1) ret = retP;
		}
	}
	return ret;
}*/

Eigen::MatrixXd Universe::getState1(Eigen::MatrixXd currentState, Eigen::MatrixXd goal){
	Eigen::MatrixXd state(1, currentState.cols());
	Eigen::MatrixXd state_prev(1, currentState.cols());
	Eigen::MatrixXd delta = goal - currentState;
	delta = delta / delta.norm();   //  unit vector
	state = goal;
	int iter = (state - currentState).norm()*100;
	fcl::ContinuousCollisionRequest requestc(iter);
    fcl::ContinuousCollisionResult resultc;
	std::vector<fcl::CollisionGeometry*> robCol = robot->getModel();
	std::vector<fcl::CollisionGeometry*> envCol = env->getModel();
	std::vector<fcl::Transform3f> robTr = robot->getTransform(currentState);
	std::vector<fcl::Transform3f> robTrNext = robot->getTransform(state);
	std::vector<fcl::Transform3f> envTr = env->getTransform();
	boost::shared_ptr<fcl::CollisionGeometry>robCol1(robCol[0]);
	boost::shared_ptr<fcl::TranslationMotion> pom(new fcl::TranslationMotion(robTr[0], robTrNext[0]));

	fcl::ContinuousCollisionObject* obj3 = new fcl::ContinuousCollisionObject(robCol1, pom);
	fcl::ContinuousCollisionObject* obj4;
	Eigen::MatrixXd ret = state;
	Eigen::MatrixXd retP = state;
	int j = 0;
	for(int i = 0; i < envCol.size(); i++){
		obj4 = new fcl::ContinuousCollisionObject(boost::shared_ptr<fcl::CollisionGeometry>(envCol[i]), boost::shared_ptr<fcl::TranslationMotion>(new fcl::TranslationMotion(envTr[i], envTr[i])));
		fcl::collide(obj3, obj4, requestc, resultc);
		if(resultc.is_collide){
			j++;
			fcl::Vec3f pomT = resultc.contact_tf1.getTranslation();
			retP(0, 0) = pomT[0];
			retP(0, 1) = pomT[1];
			if((ret-currentState).norm() > (retP - currentState).norm() || j==1) ret = retP;
		}
	}
	return ret;
}
int Universe::getCollisionCheck(){
	return colCheck;
}

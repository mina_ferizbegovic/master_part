#include "cylinder.h"

Cylinder::Cylinder(double x1, double y1, double z1, double r, double h, double angle, bool inside){
	this->r = r;
	this->x1 = x1;
	this->y1 = y1;
	this->z1 = z1;
	this->h = h;
	this->angle = angle;
	this->inside = inside;
	cylinder =  new fcl::Cylinder(r, h);
	fcl::Matrix3f R;
	R.setIdentity();
	fcl::Vec3f T(x1, y1, z1);
	transform = fcl::Transform3f(R, T);
}

fcl::CollisionGeometry* Cylinder::getModel(){
	return cylinder;
}

fcl::Transform3f Cylinder::getTransform(){
	return transform;
}

void Cylinder::plotObstacle(){
}

std::vector<std::vector<double> >Cylinder::getPlotData(){
	int n = 100;
	std::vector<double> x(n + 1), y(n + 1);
	for(int i = 0; i <= n; i++) {
		x.at(i) = r * cos(2 * M_PI * i / n) + x1;
		y.at(i) = r * sin(2 * M_PI * i / n) + y1;
	}
	std::vector<std::vector<double> >temp;
	temp.push_back(x);
	temp.push_back(y);
	return temp;
}
fcl::CollisionObject* Cylinder::getObject(){
	return new fcl::CollisionObject(boost::shared_ptr<fcl::CollisionGeometry>(cylinder), transform);
}

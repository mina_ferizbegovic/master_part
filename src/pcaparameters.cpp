#include "pcaparameters.h"

PCAparameters::PCAparameters(){
}

PCAparameters::PCAparameters(Eigen::MatrixXd val, Eigen::MatrixXd vec, Eigen::MatrixXd avg){
	eigValues = val;
	eigVectors = vec;
	average = avg;
	valid = 1;
}

Eigen::MatrixXd PCAparameters::getEigValues(){
	return eigValues;
}

Eigen::MatrixXd PCAparameters::getEigVectors(){
	return eigVectors;
}

Eigen::MatrixXd PCAparameters::getAverage(){
	return average;
}

double PCAparameters::getSmallAxis(){
	return eigValues.minCoeff();
}

double PCAparameters::getBigAxis(){
	return eigValues.maxCoeff();
}

void PCAparameters::printParam(){
	std::cout<<eigValues<<std::endl;
	std::cout<<eigVectors<<std::endl;
	std::cout<<average<<std::endl;
	
}

bool PCAparameters::isValid(){
	return valid;
}

bool PCAparameters::isNarrowPassage(){
	return narrow_passage;
}

void PCAparameters::setNarrowPassage(){
	narrow_passage = 1;
}

void PCAparameters::discardNarrowPassage(){
	narrow_passage = 0;
}
void PCAparameters::setAverage(Eigen::MatrixXd avg){
	average = avg;
}
void PCAparameters::discard(){
	useful = 0;
}
bool PCAparameters::isUseful(){
	return useful;
}

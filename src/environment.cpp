#include "environment.h"

Environment::Environment(){
}

Environment::Environment(Eigen::MatrixXd maxL, Eigen::MatrixXd minL){
	maxLimit = maxL;
	minLimit = minL;
}

void Environment::setEnvironment(std::string map){
	std::string line;
	std::string map_name = map + ".txt";
	std::ifstream file(map_name);
	if (file.is_open())
	{
	while (getline(file,line))
	{
		std::istringstream iss(line);
		std::string word;
		iss >> word;
		std::string temp = word;
		std::vector<double> values;
		while(iss >> word) {
			values.push_back(std::stod (word));
		}
		if(temp.compare("Rectangle") == 0) 
			setRectangle2D(values.at(0), values.at(1), values.at(2), values.at(3), 0);
		else if(temp.compare("Circle") == 0)
			setCircle(values.at(0), values.at(1), values.at(2), values.at(3));
		else if(temp.compare("Sphere") == 0)
			setSphere(values.at(0), values.at(1), values.at(2), values.at(3), 0);
		else if(temp.compare("Triangle") == 0)
			setTriangle(values.at(0), values.at(1), values.at(2), values.at(3), values.at(4), values.at(5));
		else if(temp.compare("Cuboid") == 0)
			setCuboid(values.at(0), values.at(1), values.at(2), values.at(3), values.at(4), values.at(5));
		else if(temp.compare("Cylinder") == 0)
			setCylinder(values.at(0), values.at(1), values.at(2), values.at(3), values.at(4), values.at(5), values.at(6));
		else
			std::cout << "Invalid input" << std::endl;
	}
	file.close();
	}
	else std::cout << "Unable to open file" << std::endl; 
}

void Environment::setRectangle2D(double x1, double y1, double a, double b, double angle = 0){
	Rectangle* rec = new Rectangle(x1, y1, a, b);
	obstacles.push_back(rec);
	env.push_back(rec->getModel());
	obj.push_back(rec->getObject());
	transform.push_back(rec->getTransform());
	insides.push_back(0);
}

std::vector<fcl::CollisionGeometry* > Environment::getModel(){
	return env;
}

std::vector<fcl::CollisionObject* > Environment::getObject(){
	return obj;
}

std::vector<bool> Environment::getIfInside(){
	return insides;
}


void Environment::plotEnv(){
}

std::vector<fcl::Transform3f> Environment::getTransform(){
	return transform;
}

std::vector<std::vector<std::vector<double> > > Environment::getPlotData(){
	std::vector<std::vector<std::vector<double> > > temp;
	for (int i = 0; i < obstacles.size(); i++){
		temp.push_back(obstacles.at(i)->getPlotData());
	}
	return temp;
}

bool Environment::checkEnvironmentFrame(Eigen::MatrixXd state){
	for(int i = 0; i < state.cols(); i++){
		if(state(0, i) > maxLimit(0, i)|| state(0, i) < minLimit(0, i)) return false;
	}
	return true;
}

Eigen::MatrixXd Environment::getMinLimit(){
	return minLimit;
}
Eigen::MatrixXd Environment::getMaxLimit(){
	return maxLimit;
}

void Environment::setCircle(double x1, double y1, double r, bool inside){
	Circle* circ = new Circle(x1, y1, r, inside);
	obstacles.push_back(circ);
	env.push_back(circ->getModel());
	obj.push_back(circ->getObject());
	transform.push_back(circ->getTransform());
	insides.push_back(inside);
}

void Environment::setTriangle(double x1, double y1, double x2, double y2, double x3, double y3){
	Triangle* tr = new Triangle(x1, y1, x2, y2, x3, y3);
	obstacles.push_back(tr);
	env.push_back(tr->getModel());
	obj.push_back(tr->getObject());
	transform.push_back(tr->getTransform());
	insides.push_back(0);
}

void Environment::setCuboid(double x1, double y1, double z1, double a, double b, double c){
	Cuboid* rec = new Cuboid(x1, y1, z1, a, b, c);
	obstacles.push_back(rec);
	env.push_back(rec->getModel());
	obj.push_back(rec->getObject());
	transform.push_back(rec->getTransform());
	insides.push_back(0);
}

void Environment::setSphere(double x1, double y1, double z1, double r, bool inside){
	Sphere* circ = new Sphere(x1, y1, z1, r, inside);
	obstacles.push_back(circ);
	env.push_back(circ->getModel());
	obj.push_back(circ->getObject());
	transform.push_back(circ->getTransform());
	insides.push_back(inside);
}

void Environment::setCylinder(double x1, double y1, double z1, double r, double h, double angle, bool inside){
	Cylinder* circ = new Cylinder(x1, y1, z1, r, h, angle, inside);
	obstacles.push_back(circ);
	env.push_back(circ->getModel());
	obj.push_back(circ->getObject());
	transform.push_back(circ->getTransform());
	insides.push_back(inside);
}

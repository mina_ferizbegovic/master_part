#include "tree.h"
Tree::Tree(double maxIter) {
	allPoints.reserve(100000);
	allPoints_np.reserve(100000);
	allPoints_bitno.reserve(100000);
}

Tree::~Tree() {
    reset();
}

void Tree::reset() {
    if (!nodes.empty()) {
        for (Node* n : nodes)
            delete n;
        nodes.clear();
    }
}

std::vector<Eigen::MatrixXd> Tree::getPath(Node* dest, bool reverse) {
    std::vector<Eigen::MatrixXd> vectorOut;
    Node* node = dest;
	while(node) {
		vectorOut.push_back(node->getConfigState());
		node = node->getParent();
	}
    return vectorOut;
}

Node* Tree::getFirstNode(){
    if (nodes.empty()) return nullptr;
    return nodes.front();
}

Node* Tree::getLastNode(){
    if (nodes.empty()) return nullptr;
    return nodes.back();
}

std::vector<Node* > Tree::getAllNodes(){
    return nodes;
}

Eigen::MatrixXd Tree::startPosition(){
    if (nodes.empty()) throw("No start state specified for tree");
    else return getFirstNode()->getConfigState();
}

void Tree::setStartPosition(Eigen::MatrixXd startPosition) {
    Node* root = new Node(startPosition, nullptr);
    addNewNode(root);
    allPoints.push_back(startPosition);
}

void Tree::addNewNode(Node *newNode) {
	last = newNode;
	node_hash.insert({newNode->getConfigState(), nodes.size()});
    nodes.push_back(newNode);
    allPoints.push_back(newNode->getConfigState());
}

void Tree::addNewNode(Eigen::MatrixXd state, Node* parent){
	allPoints.push_back(state);
	Node* temp = new Node(state, parent);
	addNewNode(temp);
}

void Tree::addNewNode(Node* newNode, int index){
	last = newNode;
	node_hash.insert({newNode->getConfigState(), nodes.size()});
    nodes.push_back(newNode);
    allPoints[index] = newNode->getConfigState();
}

Node* Tree::getNode(Eigen::MatrixXd a) {
    iterHash = node_hash.find(a);
	if ( iterHash == node_hash.end() ){
	  return nullptr;
	}
    return nodes[iterHash->second];
}

int Tree::getIndex(Eigen::MatrixXd a) {
    iterHash = node_hash.find(a);
	if ( iterHash == node_hash.end() ){
	  return -1;
	}
    return iterHash->second;
}
std::vector<Eigen::MatrixXd> Tree::getPath(){
	std::vector<Eigen::MatrixXd> v;
	while(last){
		v.push_back(last->getConfigState());
		last = last->getParent();
	}
	std::reverse(v.begin(), v.end());
	allPoints.clear();
    std::vector<Eigen::MatrixXd>().swap(allPoints);
	allPoints_np.clear();
	std::vector<Eigen::MatrixXd>().swap(allPoints_np);
	allPoints_bitno.clear();
	std::vector<Eigen::MatrixXd>().swap(allPoints_bitno);
	return v;
}

std::vector<Eigen::MatrixXd>* Tree::getAllPoints(){
	return &allPoints;
}
std::vector<Eigen::MatrixXd>* Tree::getAllPointsBitno(){
	return &allPoints_bitno;
}
std::vector<Eigen::MatrixXd>* Tree::getAllPointsNP(){
	return &allPoints_np;
}
std::vector<Eigen::MatrixXd>* Tree::getAllPointsOBST(){
	return &allPoints_obst;
}

void Tree::addNPpoint(Eigen::MatrixXd s){
	int n = 1;
	allPoints_np.push_back(s);
	if(getAllPointsNP()->size() < 2){
		index_np = new Index<L2<double> >(flann::Matrix<double>(&(getAllPointsNP())->back()(0,0), 1, s.cols()), flann::KDTreeIndexParams(1));
		index_np->buildIndex();
		indices_np = new flann::Matrix<int> (new int[n], 1, n);
		dists_np = new flann::Matrix<double> (new double[n], 1, n);
	}
	else
		index_np->addPoints(flann::Matrix<double>(&((getAllPointsNP())->back())(0,0), 1, s.cols()));
}

void Tree::addBITNOpoint(Eigen::MatrixXd s){
	int n = 1;
	allPoints_bitno.push_back(s);
	if(getAllPointsBitno()->size() < 2){
		index_bitno = new Index<L2<double> >(flann::Matrix<double>(&(getAllPointsBitno())->back()(0,0), 1, s.cols()), flann::KDTreeIndexParams(1));
		index_bitno->buildIndex();
		indices_bitno = new flann::Matrix<int> (new int[n], 1, n);
		dists_bitno = new flann::Matrix<double> (new double[n], 1, n);
	}
	else
		index_bitno->addPoints(flann::Matrix<double>(&((getAllPointsBitno())->back())(0,0), 1, s.cols()));
}
void Tree::addOBSTpoint(Eigen::MatrixXd s){
	allPoints_obst.push_back(s);
}

PCAparameters Tree::getPcaParam(Eigen::MatrixXd currentState){
	pcaHash = pca_hash.find(currentState);
	if (pcaHash == pca_hash.end()){
		return PCAparameters();
	}
	return pcaHash->second;
}

void Tree::setPcaParam(Eigen::MatrixXd currentState, PCAparameters param){
	pca_hash[currentState] = param;
}

void Tree::addPcaParam(Eigen::MatrixXd currentState, PCAparameters param){
	pca_hash.insert({currentState, param});
}

std::vector<Eigen::MatrixXd> Tree::getEndPoints(Eigen::MatrixXd currentState){
	std::vector<Eigen::MatrixXd> temp;
	endpointsHash = endpoints_hash.find(currentState);
	if (endpointsHash == endpoints_hash.end()){
		return temp;
	}
	return endpointsHash->second;
}
void Tree::addEndPoints(Eigen::MatrixXd currentState, std::vector<Eigen::MatrixXd> vec){
	endpoints_hash.insert({currentState,vec});
}

void Tree::setLastNode(Node* node){
	last = node;
}

std::unordered_map<Eigen::MatrixXd, PCAparameters, matrix_hash<Eigen::MatrixXd> > Tree::getPCAhash(){
	return pca_hash;
}
bool Tree::ifKExists(Eigen::MatrixXd s){
	kHash = k_hash.find(s);
	if (kHash == k_hash.end()){
		return false;
	}
	return true;
}
Eigen::MatrixXd Tree::getK(Eigen::MatrixXd s){
	kHash = k_hash.find(s);
	return kHash->second;
}
void Tree::writeK(Eigen::MatrixXd a, Eigen::MatrixXd delta){
	k_hash.insert({a, delta});
}
std::vector<Eigen::MatrixXd>* Tree::getAllPointsMean(){
	return &allPoints_mean;
}
void Tree::addMeanpoint(Eigen::MatrixXd s){
	allPoints_mean.push_back(s);
}

Eigen::MatrixXd Tree::findNearest_np(Eigen::MatrixXd randNode){
	int n = 1;
	Eigen::MatrixXd sth(1, 2);
	sth<<-1, -1;
	if(getAllPointsNP()->size() < 1) return sth;
	index_np->knnSearch(flann::Matrix<double>(&randNode(0, 0), 1, randNode.cols()), *indices_np, *dists_np, 1, flann::SearchParams(128));
	double* uu = index_np->getPoint(*indices_np[0][0]);

	Eigen::MatrixXd temp(1, randNode.cols());
	for (int i = 0; i < randNode.cols(); i++){
		temp(0, i) = *uu;
		uu++;
	}
	return temp;
}

Eigen::MatrixXd Tree::findNearest_bitno(Eigen::MatrixXd randNode){
	int n = 1;
	Eigen::MatrixXd sth(1, 2);
	sth<<-1, -1;
	if(getAllPointsBitno()->size() < 1) return sth;
	index_bitno->knnSearch(flann::Matrix<double>(&randNode(0, 0), 1, randNode.cols()), *indices_bitno, *dists_bitno, 1, flann::SearchParams(128));
	double* uu = index_bitno->getPoint(*indices_bitno[0][0]);

	Eigen::MatrixXd temp(1, randNode.cols());
	for (int i = 0; i < randNode.cols(); i++){
		temp(0, i) = *uu;
		uu++;
	}
	return temp;
}

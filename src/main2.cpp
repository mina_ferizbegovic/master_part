#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <iostream>
#include "robot.h"
#include "pointRobot.h"
#include "universe.h"
#include "environment.h"
#include "tree.h"
#include "rrtpca.h"
#include "rrtsimple.h"
#include "planararm.h"
#include <time.h>
#include "rrtcom.h"
#include "rrtlast.h"
#include "rrtnp.h"
#include "rrtsimple.h"
#include "rrtver2.h"
#include "rrtbase3.h"
#include "rrtbase2.h"
#include "rrtbase1.h"

int main()
{
	srand(time(0));
	std::ofstream myfile;
	myfile.open("example2.txt");

	//define robot
		//double res = 0;
	Robot* p = new PointRobot(0.001);

	Eigen::MatrixXd A = Eigen::MatrixXd::Random(1000000, 2) * 5 + Eigen::MatrixXd::Ones(1000000, 2) * 5;
	//define environment
	double pi = 3.14;
	Eigen::MatrixXd c(1, 2), d(1, 2), e(1,3), f(1,3), g(1,3); //limits for the environment
	c<<10,10;
	d<<0,0;
	e<<pi, pi, pi;
	f<<-pi, -pi, -pi;
	g<<1,1,1;
	//Robot* p = new PlanarArm(g, e, f);
	Environment* env = new Environment(c, d);
	std::string mapa = "mapa12"; //determine map
	env->setEnvironment(mapa);

	//define universe
	Universe* s = new Universe(env, p);

	//choose algorithm (RRTsimple or PCA-RRT)
	RRTbase3* rrt ;

	double res = 0;
	Eigen::MatrixXd a(1, 2);
	Eigen::MatrixXd b(1, 2);
	for(int k = 0; k < 1; k++){
		rrt = new RRTbase3();
		//RRTPCA* rrt = new RRTPCA();
		rrt->setUniverse(s);
		//rrt->setPoints(A);

		// define start and goal state
		/*Eigen::MatrixXd a(1, 3);
		Eigen::MatrixXd b(1, 3);
		a<<0,0,0;
		b<<pi/2,0,0;*/

		a<<3.05, 5.1;
		b<<8.8, 5;
		rrt->setStartState(a);
		rrt->setGoalState(b);

		//calculate time

		clock_t start = clock();
		std::cout<<rrt->run()<<std::endl;
		clock_t stop = clock();
		double elapsed = (double)(stop - start) * 1000.0 / CLOCKS_PER_SEC;
		std::cout<< k<< std::endl;
		std::cout<< elapsed<< std::endl;
		myfile<<k<<" "<<elapsed<<" "<<rrt->getNodeNum()<<" "<<rrt->getCollisionChecking()<<" "<< rrt->getCollisionChecking() - rrt->getNodeNum()<<" "<<rrt->getNPenter()<<" "<<rrt->getPcaNum()<<std::endl;
		res = res + elapsed;
		s->resetCollisionCheck();
	}

	std::cout<<res/30<< std::endl;

	rrt->plotRRTPath();
	rrt->print();
	myfile.close();

    return 0;

}

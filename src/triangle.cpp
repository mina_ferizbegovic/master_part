#include "triangle.h"

Triangle::Triangle(double x1, double y1, double x2, double y2, double x3, double y3){
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->x3 = x3;
	this->y3 = y3;
	fcl::BVHModel<fcl::OBBRSS>* model = new fcl::BVHModel<fcl::OBBRSS>();
	fcl::Vec3f a(x1, y1, 0);
	fcl::Vec3f b(x2, y2, 0);
	fcl::Vec3f c(x3, y3, 0);
    model->beginModel();
    model->addTriangle(a, b, c);
    model->endModel();
	transform = fcl::Transform3f();
    triangle = new fcl::CollisionObject(boost::shared_ptr<fcl::CollisionGeometry>(model), transform);
}

fcl::CollisionObject* Triangle::getObject(){
	return triangle;
}

fcl::CollisionGeometry* Triangle::getModel(){
	return new fcl::Sphere(1);
}

fcl::Transform3f Triangle::getTransform(){
	return transform;
}

void Triangle::plotObstacle(){
}

std::vector<std::vector<double> > Triangle::getPlotData(){
	std::vector<double> x(6), y(6);
	
	x.at(0) = x1;
	y.at(0) = y1;
	x.at(1) = x2;
	y.at(1) = y2;
	x.at(2) = x2;
	y.at(2) = y2;
	x.at(3) = x3;
	y.at(3) = y3;
	x.at(4) = x3;
	y.at(4) = y3;
	x.at(5) = x1;
	y.at(5) = y1;

	std::vector<std::vector<double> >temp;
	temp.push_back(x);
	temp.push_back(y);
	return temp;
}


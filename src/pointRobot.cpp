#include "pointRobot.h"

PointRobot::PointRobot(double r){
	this->r = r;
}
Eigen::MatrixXd PointRobot::toWorkSpace(Eigen::MatrixXd configSpace){
	return configSpace;
}
Eigen::MatrixXd PointRobot::toConfigSpace(Eigen::MatrixXd workSpace){
	return workSpace;
}
double PointRobot::getR() const{
	return r;
}
std::vector<fcl::CollisionGeometry*> PointRobot::getModel(){
	fcl::CollisionGeometry* s = new fcl::Sphere(r);
	std::vector<fcl::CollisionGeometry*> temp;
	temp.push_back(s);
	return temp;
}
Eigen::MatrixXd PointRobot::getRandomConfig(){
	Eigen::MatrixXd temp(1, 2);
	temp(0, 0) = rand() / (float)RAND_MAX * (q1Max - q1Min - 2 * r) + q1Min + r;
	temp(0, 1) = rand() / (float)RAND_MAX * (q2Max - q2Min - 2 * r) + q2Min + r;
	//temp(0, 2) = rand() / (float)RAND_MAX * (q3Max - q3Min - 2 * r) + q3Min + r;
	return temp; 
}

Eigen::MatrixXd PointRobot::getRandomConfig(double R){
	Eigen::MatrixXd temp(1, 2);
	temp(0, 0) = rand() / (float)RAND_MAX * R + q1Min + r;
	temp(0, 1) = rand() / (float)RAND_MAX * R + q2Min + r;
	//temp(0, 2) = rand() / (float)RAND_MAX * R + q3Min + r;
	return temp; 
}
std::vector<fcl::Transform3f> PointRobot::getTransform(Eigen::MatrixXd state){
	fcl::Matrix3f R;
	R.setIdentity();
	fcl::Vec3f T(state(0, 0), state(0, 1), 0);
	std::vector<fcl::Transform3f> temp;
	fcl::Transform3f tr(R, T);
	temp.push_back(tr);
	return temp;
 }
 
Eigen::MatrixXd  PointRobot::getNRandomConfig(int N, double dist){
	Eigen::MatrixXd temp(N, 2);
	return temp; 
}
int PointRobot::getDimension(){
	return 3;
}

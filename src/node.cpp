#include "node.h"

Node::Node(){
	parent = nullptr;
}

Node::Node(Eigen::MatrixXd state){
	configState = state;
	parent = nullptr;
}

Node::Node(Eigen::MatrixXd state, Node* par){
	configState = state;
	parent = par;
}

Node* Node::getParent(){
	return parent;
}

Eigen::MatrixXd Node::getConfigState(){
	return configState;
}

void Node:: setParent(Node* par){
	parent = par;
}

void Node:: setState(Eigen::MatrixXd st){
	configState = st;
}
void Node::setNP(int np){
	this->np = np;
}

int Node::ifNP(){
	return np;
}

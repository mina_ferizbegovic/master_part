#include "planner.h"

Eigen::MatrixXd Planner::getStartState(){
	return startState;
}

Eigen::MatrixXd Planner::getGoalState(){
	return goalState;
}

Universe* Planner::getUniverse(){
	return universe;
}

void Planner::setUniverse(Universe* u){
	universe = u;
}

double Planner::getThreshold(){
	return goalThresholdDistance;
}

void Planner::setThreshold(double t){
	goalThresholdDistance = t;
}

int Planner::getMaxIterations(){
	return maxIterations;
}

void Planner::setMaxIterations(int t){
	maxIterations = t;
}

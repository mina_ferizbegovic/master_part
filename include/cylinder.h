#ifndef CYLINDER_H
#define CYLINDER_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"
#include "obstacle.h"

class Cylinder : public Obstacle{
		double x1;
		double y1;
		double z1;
		double r;
		double h;
		double angle;
		bool inside;
		fcl::CollisionGeometry* cylinder;
		fcl::Transform3f transform;
	public:
		Cylinder(double x1, double y1, double z1, double r, double h, double angle, bool inside);
		fcl::CollisionGeometry* getModel();
		fcl::CollisionObject* getObject();
		fcl::Transform3f getTransform();
		void plotObstacle();
		std::vector<std::vector<double> > getPlotData();
};
#endif

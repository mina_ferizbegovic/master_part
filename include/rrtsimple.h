// This class is implementing standard (simple) RRT and is extension of RRTver(RRT version) class.
#ifndef RRTSIMPLE_H
#define RRTSIMPLE_H

#include "planner.h"
#include "tree.h"
#include "eigen3/Eigen/Dense"
#include <flann/flann.h>
#include "rrtver.h"

class RRTsimple : public RRTver{
		double stepSize;
	public:
		RRTsimple(double step);
		std::vector<Node*> extend(Eigen::MatrixXd target, Node* nearestNode);
		std::vector<Node*> extend_np(Node* nearestNode);
		std::vector<Node*> extend1(Node* nearestNode);
		std::vector<Node*> dod_tree(Node* nearestNode);
};
#endif


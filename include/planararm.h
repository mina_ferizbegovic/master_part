//This class is implementation of n-segment planar arm and it is extension of class Robot.
#ifndef PLANARARM_H
#define PLANARARM_H

#include "robot.h"
#include "eigen3/Eigen/Dense"
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"
#include <vector>

class PlanarArm : public Robot{
		double r = 0.1;
		Eigen::MatrixXd maxLimits;
		Eigen::MatrixXd minLimits;
		Eigen::MatrixXd lengthSeg;
		double width = 1;
	public:
		PlanarArm(Eigen::MatrixXd len, Eigen::MatrixXd maxL, Eigen::MatrixXd minL);
		Eigen::MatrixXd toWorkSpace(Eigen::MatrixXd configSpace);
		Eigen::MatrixXd  toConfigSpace(Eigen::MatrixXd workSpace);
		std::vector<fcl::CollisionGeometry*> getModel();
		std::vector<fcl::Transform3f> getTransform(Eigen::MatrixXd state);
		Eigen::MatrixXd  getRandomConfig();
		fcl::Matrix3f getRotationMatrix(double angle);
		Eigen::MatrixXd  getNRandomConfig(int N, double dist);
		int getDimension();
		Eigen::MatrixXd getRandomConfig(double r);
};
#endif

#ifndef RECTANGLE_H
#define RECTANGLE_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"
#include "obstacle.h"

class Rectangle : public Obstacle{
		double x1;
		double y1; 
		double a;
		double b;
		fcl::CollisionGeometry* box;
		fcl::Transform3f transform;
	public:
		Rectangle(double x1, double y1, double a, double b);
		fcl::CollisionGeometry* getModel();
		fcl::CollisionObject* getObject();
		fcl::Transform3f getTransform();
		void plotObstacle();
		std::vector<std::vector<double> > getPlotData();
};
#endif

//This class is implementation of point robot. 
#ifndef POINTROBOT_H
#define POINTROBOT_H

#include "robot.h"

class PointRobot : public Robot{
		double r = 0.01;
		double q1Max = 10;
		double q1Min = 0;
		double q2Max = 10;
		double q2Min = 0;
		double q3Max = 10;
		double q3Min = 0;
	public:
		PointRobot(double r);
		Eigen::MatrixXd toWorkSpace(Eigen::MatrixXd configSpace);
		Eigen::MatrixXd  toConfigSpace(Eigen::MatrixXd workSpace);
		std::vector<fcl::CollisionGeometry*> getModel();
		std::vector<fcl::Transform3f> getTransform(Eigen::MatrixXd state);
		Eigen::MatrixXd  getRandomConfig();
		Eigen::MatrixXd  getRandomConfig(double R);
		Eigen::MatrixXd  getNRandomConfig(int N, double dist);
		double getR() const;
		int getDimension();
};
#endif

//This class describes a free and occupied space.
#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"
#include "obstacle.h"
#include "rectangle.h"
#include "circle.h"
#include "triangle.h"
#include "cylinder.h"
#include "cuboid.h"
#include "sphere.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

class Environment{
		std::vector<fcl::CollisionGeometry* > env;
		std::vector<fcl::CollisionObject* > obj;
		std::vector<fcl::Transform3f> transform;
		std::vector<Obstacle*> obstacles;
		std::vector<bool>insides;
		Eigen::MatrixXd maxLimit;
		Eigen::MatrixXd minLimit;
	public:
		Environment();
		std::vector<bool> getIfInside();
		Environment(Eigen::MatrixXd maxL, Eigen::MatrixXd minL);
		void setRectangle2D(double x1, double y1, double a, double b, double angle);
		void setCylinder(double x1, double y1, double z1, double r, double h, double angle, bool inside);
		void setCuboid(double x1, double y1, double z1, double a, double b, double c);
		void setCircle(double x1, double y1, double r, bool inside);
		void setSphere(double x1, double y1, double z1, double r, bool inside);
		void setEllipse(double xc, double yc, double a, double b);
		void setTriangle(double x1, double y1, double x2, double y2, double x3, double y3);
		std::vector<fcl::CollisionGeometry* > getModel();
		std::vector<fcl::CollisionObject* > getObject();
		std::vector<fcl::Transform3f> getTransform();
		void plotEnv();
		std::vector<std::vector<std::vector<double> > > getPlotData();
		bool checkEnvironmentFrame(Eigen::MatrixXd state);
		void setEnvironment(std::string mapa);
		Eigen::MatrixXd getMinLimit();
		Eigen::MatrixXd getMaxLimit();
};
#endif

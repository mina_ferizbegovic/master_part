#ifndef PCAPARAMETERS_H
#define PCAPARAMETERS_H

#include <eigen3/Eigen/Dense>
#include <iostream>

class PCAparameters {
		Eigen::MatrixXd eigValues;
		Eigen::MatrixXd eigVectors;
		Eigen::MatrixXd average;
		bool valid = 0;
		bool useful = 1;
		bool narrow_passage = 0;
		
	public:
		PCAparameters();
		PCAparameters(Eigen::MatrixXd val, Eigen::MatrixXd vec, Eigen::MatrixXd avg);
		Eigen::MatrixXd getEigValues();
		Eigen::MatrixXd getEigVectors();
		Eigen::MatrixXd getAverage();
		double getSmallAxis();
		double getBigAxis();
		void setAverage(Eigen::MatrixXd avg);
		void printParam();
		bool isValid();
		bool isNarrowPassage();
		void setNarrowPassage();
		void discardNarrowPassage();
		void discard();
		bool isUseful();
};

#endif

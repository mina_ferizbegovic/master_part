//A node is the leaf of a tree.
#ifndef NODE_H
#define NODE_H

#include <eigen3/Eigen/Dense>

class Node{
		Eigen::MatrixXd configState;
		Node* parent;
		int np = 0;
	public:
		Node();
		Node(Eigen::MatrixXd state);
		Node(Eigen::MatrixXd state, Node* par);
		Node* getParent();
		Eigen::MatrixXd getConfigState();
		void setParent(Node* par);
		void setState(Eigen::MatrixXd st);
		void setNP(int np);
		int ifNP();
};

#endif

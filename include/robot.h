//This class is abstract class that is base for every robot implementation.
#ifndef ROBOT_H
#define ROBOT_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"

class Robot{
	public:
	      virtual Eigen::MatrixXd toWorkSpace(Eigen::MatrixXd configSpace) = 0;
	      virtual Eigen::MatrixXd toConfigSpace(Eigen::MatrixXd workSpace) = 0;
	      virtual std::vector<fcl::CollisionGeometry*> getModel() = 0;
	      virtual std::vector<fcl::Transform3f> getTransform(Eigen::MatrixXd state) = 0;
	      virtual Eigen::MatrixXd getRandomConfig() = 0;
	      virtual Eigen::MatrixXd getRandomConfig(double r) = 0;
	      virtual Eigen::MatrixXd getNRandomConfig(int N, double dist) = 0;
	      virtual int getDimension() = 0;
};
#endif

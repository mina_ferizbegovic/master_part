#ifndef OBSTACLE_H
#define OBSTACLE_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"

class Obstacle{
	protected:
		bool inside = 0;
	public:
		  virtual fcl::CollisionGeometry* getModel() = 0;
		  virtual fcl::CollisionObject* getObject() = 0;
		  virtual fcl::Transform3f getTransform() = 0;
		  virtual void plotObstacle() = 0;
		  virtual std::vector<std::vector<double> > getPlotData() = 0;
		  bool getInside();
};
#endif

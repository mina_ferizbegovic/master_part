/*This class is base for all RRT version algorithms. 
As standard RRT and PCA-RRT differ only in calculating new state,
this class doesn't implement this method (abstract).*/

#ifndef RRTVER_H
#define RRTVER_H

#include "planner.h"
#include "tree.h"
#include "eigen3/Eigen/Dense"
#include <flann/flann.h>
#include <math.h> 
#include <fstream>

class RRTver : public Planner{
	protected:
		flann::Index<flann::L2<double> > *index;
		flann::Matrix<int> *indices;
		flann::Matrix<double> *dists;
		Tree* tree = new Tree(maxIterations);
		Node* help_node = new Node();
		bool nap = 0;
		bool dod = 0;
		bool ror = 0;
		Eigen::MatrixXd A;
		int stopIter = 0;
		int iter = 0;
		int obst = 0;
		std::string name = "./rrt.png";
		std::vector<Eigen::MatrixXd> zlj;
		int npIter = -1;
	public:
		bool run();
		std::vector<Node*> grow();
		Eigen::MatrixXd getRandomConfig();
		Node* findNearest(Eigen::MatrixXd randNode);
		virtual std::vector<Node*> extend(Eigen::MatrixXd target, Node* nearestNode) = 0;
		virtual std::vector<Node*> extend_np(Node* nearestNode) = 0;
		virtual std::vector<Node*> extend1(Node* nearestNode) = 0;
		virtual std::vector<Node*> dod_tree(Node* nearestNode) = 0;
		void plotRRTPath2D();
		void plotRRTPath3D();
		void plotRRTPath();
		std::vector<Eigen::MatrixXd> getConfigSpacePath();
		std::vector<Eigen::MatrixXd> getWorkSpacePath();
		void setStartState(Eigen::MatrixXd start);
		void setGoalState(Eigen::MatrixXd start);
		void setPoints(Eigen::MatrixXd B);
		int getStopIter();
		void setName(std::string a);
		void print();
		int getNodeNum();
		int getRejected();
		int getCollisionChecking();
		int getNPenter();
};
#endif


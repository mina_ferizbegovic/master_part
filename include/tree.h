//This class implements structure of a tree that is used for keeping information of a node state and his parent.
#ifndef TREE_H
#define TREE_H
#include <vector>
#include "node.h"
#include <unordered_map>
#include <iostream>
#include "pcaparameters.h"
#include <flann/flann.h>

//hash function Eigen::MatrixXd
template<typename T>
struct matrix_hash : std::unary_function<T, size_t> {
  std::size_t operator()(T const& matrix) const {
    // Note that it is oblivious to the storage order of Eigen matrix (column- or
    // row-major). It will give you the same hash value for two different matrices if they
    // are the transpose of each other in different storage order.
    size_t seed = 0;
    for (size_t i = 0; i < matrix.size(); ++i) {
      auto elem = *(matrix.data() + i);
      seed ^= std::hash<typename T::Scalar>()(elem) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }
    return seed;
  }
};

class Tree {
    std::vector<Node*> nodes;
    Node* last;
    flann::Index<flann::L2<double> > *index_np;
    flann::Matrix<int> *indices_np;
    flann::Matrix<double> *dists_np;
    flann::Index<flann::L2<double> > *index_bitno;
    flann::Matrix<int> *indices_bitno;
    flann::Matrix<double> *dists_bitno;
    std::unordered_map<Eigen::MatrixXd, int, matrix_hash<Eigen::MatrixXd> >node_hash;
    std::unordered_map<Eigen::MatrixXd, int, matrix_hash<Eigen::MatrixXd> >::const_iterator iterHash;
    std::unordered_map<Eigen::MatrixXd, Eigen::MatrixXd, matrix_hash<Eigen::MatrixXd> >k_hash;
    std::unordered_map<Eigen::MatrixXd, Eigen::MatrixXd, matrix_hash<Eigen::MatrixXd> >::const_iterator kHash;
    std::unordered_map<Eigen::MatrixXd, PCAparameters, matrix_hash<Eigen::MatrixXd> >pca_hash;
    std::unordered_map<Eigen::MatrixXd, PCAparameters, matrix_hash<Eigen::MatrixXd> >::const_iterator pcaHash;
    std::unordered_map<Eigen::MatrixXd, std::vector<Eigen::MatrixXd>, matrix_hash<Eigen::MatrixXd> >endpoints_hash;
    std::unordered_map<Eigen::MatrixXd, std::vector<Eigen::MatrixXd>, matrix_hash<Eigen::MatrixXd> >::const_iterator endpointsHash;
    std::vector<Eigen::MatrixXd> allPoints;
    std::vector<Eigen::MatrixXd> allPoints_bitno;
    std::vector<Eigen::MatrixXd> allPoints_np;
    std::vector<Eigen::MatrixXd> allPoints_mean;
    std::vector<Eigen::MatrixXd> allPoints_obst;

public:
    Tree(double maxIter);
    ~Tree();
    void reset();
    std::vector<Eigen::MatrixXd> getPath(Node* dest, bool reverse = false);
    Node* getFirstNode();
    Node* getLastNode();
    void setLastNode(Node* node);
    std::vector<Node* > getAllNodes();
    Eigen::MatrixXd startPosition();
    void setStartPosition(Eigen::MatrixXd startPosition);
    void addNewNode(Node* newNode);
    void addNewNode(Eigen::MatrixXd state, Node* parent);
    void addNewNode(Node* newNode, int index);
    Node* getNode(Eigen::MatrixXd config);
    std::vector<Eigen::MatrixXd> getPath();
    std::vector<Eigen::MatrixXd>* getAllPoints();
    std::vector<Eigen::MatrixXd>* getAllPointsNP();
    std::vector<Eigen::MatrixXd>* getAllPointsBitno();
    std::vector<Eigen::MatrixXd>* getAllPointsMean();
    std::vector<Eigen::MatrixXd>* getAllPointsOBST();
    void addNPpoint(Eigen::MatrixXd s);
    void addBITNOpoint(Eigen::MatrixXd s);
    void addMeanpoint(Eigen::MatrixXd s);
    void addOBSTpoint(Eigen::MatrixXd s);
    PCAparameters getPcaParam(Eigen::MatrixXd currentState);
    void addPcaParam(Eigen::MatrixXd currentState, PCAparameters param);
	  std::vector<Eigen::MatrixXd> getEndPoints(Eigen::MatrixXd currentState);
    void addEndPoints(Eigen::MatrixXd currentState, std::vector<Eigen::MatrixXd> vec);
    std::unordered_map<Eigen::MatrixXd, PCAparameters, matrix_hash<Eigen::MatrixXd> > getPCAhash();
    int getIndex(Eigen::MatrixXd a);
    bool ifKExists(Eigen::MatrixXd s);
    Eigen::MatrixXd getK(Eigen::MatrixXd s);
    void writeK(Eigen::MatrixXd a, Eigen::MatrixXd delta);
    Eigen::MatrixXd findNearest_np(Eigen::MatrixXd randNode);
    Eigen::MatrixXd findNearest_bitno(Eigen::MatrixXd randNode);
    void setPcaParam(Eigen::MatrixXd currentState, PCAparameters param);
};



#endif

#ifndef TRIANGLE_H
#define TRIANGLE_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"
#include "obstacle.h"

class Triangle : public Obstacle{
		double x1;
		double y1;
		double x2;
		double y2;
		double x3;
		double y3;
		fcl::CollisionObject* triangle;
		fcl::Transform3f transform;
	public:
		Triangle(double x1, double y1, double x2, double y2, double x3, double y3);
		fcl::CollisionGeometry* getModel();
		fcl::CollisionObject* getObject();
		fcl::Transform3f getTransform();
		void plotObstacle();
		std::vector<std::vector<double> > getPlotData();
};
#endif

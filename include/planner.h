//This class is base for every path planning algorithm.
#ifndef PLANNER_H
#define PLANNER_H
#include "universe.h"

class Planner{
	protected:
		Universe* universe = new Universe();
		int maxIterations = 100000;
		Eigen::MatrixXd startState;
		Eigen::MatrixXd goalState;
		double goalBias = 0.1;
		double goalThresholdDistance = 0.5;

	public:
		virtual std::vector<Eigen::MatrixXd> getConfigSpacePath() = 0;
		virtual std::vector<Eigen::MatrixXd> getWorkSpacePath() = 0;
		virtual void plotRRTPath() = 0;
		Eigen::MatrixXd getStartState();
		Eigen::MatrixXd getGoalState();
		virtual void setStartState(Eigen::MatrixXd s) = 0;
		virtual void setGoalState(Eigen::MatrixXd g) = 0;
		Universe* getUniverse();
		void setUniverse(Universe* u);
		double getThreshold();
		void setThreshold(double t);
		int getMaxIterations();
		void setMaxIterations(int t);
};
#endif

#ifndef CUBOID_H
#define CUBOID_H
#include <vector>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <boost/shared_ptr.hpp>
#include <eigen3/Eigen/Dense>
#include "fcl/ccd/motion.h"
#include "fcl/narrowphase/narrowphase.h"
#include "obstacle.h"

class Cuboid : public Obstacle{
		double x1;
		double y1; 
		double z1;
		double a;
		double b;
		double c;
		fcl::CollisionGeometry* box;
		fcl::Transform3f transform;
	public:
		Cuboid(double x1, double y1, double z1, double a, double b, double c);
		fcl::CollisionGeometry* getModel();
		fcl::CollisionObject* getObject();
		fcl::Transform3f getTransform();
		void plotObstacle();
		std::vector<std::vector<double> > getPlotData();
};
#endif

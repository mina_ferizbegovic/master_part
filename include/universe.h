//THis class contains environment (description of free and occupied space) and robot.
#ifndef UNIVERSE_H
#define UNIVERSE_H
#include <vector>
#include <boost/shared_ptr.hpp>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include "eigen3/Eigen/Dense"
#include "environment.h"
#include "robot.h"
#include <fcl/continuous_collision.h>
#include <flann/flann.h>
#include "test_fcl_utility.h"

class Universe{
		Robot* robot;
		Environment* env;
		std::vector<Eigen::MatrixXd> occupiedPoints;
		flann::Index<flann::L2<double> > *index_occ;
		flann::Matrix<int> *indices_occ;
		flann::Matrix<double> *dists_occ;
		int colCheck = 0;
	public:
		Universe();
		Universe(Environment* env, Robot* rob);
		bool checkState(Eigen::MatrixXd state);
		bool checkMotionValidity(Eigen::MatrixXd currentState, Eigen::MatrixXd nextState);
		Eigen::MatrixXd getState(Eigen::MatrixXd currentState, Eigen::MatrixXd nextState, double r, Eigen::MatrixXd goal);
		Eigen::MatrixXd  getValidRandomConfig();
		Eigen::MatrixXd  getValidRandomConfig(double r);
		Eigen::MatrixXd getWorkSpace(Eigen::MatrixXd state);
		void setRobot(Robot* r);
		void setEnvironment(Environment* env);
		Robot* getRobot();
		Environment* getEnvironment();
		std::vector<Eigen::MatrixXd>* getOccupiedPoints();
		double findNearestObstacle(Eigen::MatrixXd a);
		Eigen::MatrixXd getState1(Eigen::MatrixXd currentState, Eigen::MatrixXd goal);
		int getCollisionCheck();
		void resetCollisionCheck();
};
#endif
